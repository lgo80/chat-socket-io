const path = require('path');
const express = require('express');
const app = express();
var socketsClie = [];
var room1 = [];

//settings
app.set('port', process.env.PORT || 3000);

//static file
app.use(express.static(path.join(__dirname, 'public')))

//Start the server
const server = app.listen(app.get('port'), () => {
    console.log('Server on port ', app.get('port'));
});

// websockets
const socketIO = require('socket.io');
const {
    count
} = require('console');
const io = socketIO(server);

io.on('connection', (socket) => {
    console.log('new connections', socket.id);
    io.sockets.emit('chat:connections', room1);

    socket.on('chat:enter', (data) => {
        socket.join('room1');
        io.to(socket.id).emit("chat:enter-user", data);
        socket.data.username = data.username;
        socketsClie[data.username] = {
            "socket": socket,
            "username": data.username
        };
        room1.push({
            "username": data.username
        });
        data.room = room1;
        data.message = `<p>User ${data.username} logged in to the room</>`;
        io.sockets.emit('chat:enter-out', data);
    });

    socket.on('chat:message', (data) => {
        io.to('room1').emit('chat:message', data);
    });

    socket.on('chat:typing', (username) => {
        socket.broadcast.emit('chat:typing', username);
    });

    socket.on('chat:private', (data) => {
        let roomPrivate = data.username_receiver + "-" + data.username_emitter;
        socket.join(roomPrivate);
        socketsClie[data.username_receiver].socket.join(roomPrivate);
        io.to(roomPrivate).emit('chat:private', data);
    });

    socket.on('disconnecting', () => {
        room1 = room1.filter(room => room.username != socket.data.username);
        io.sockets.emit('chat:enter-out', {
            'message': `<p>User ${socket.data.username} has left the room</>`,
            'room': room1
        });
    });
});