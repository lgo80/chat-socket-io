### Chat socket.IO

------------

*Chat designed in Html, css and node.js*

> Requirements

It must be installed:
- Node.js

> Installation steps

1. Clone the repository with "git clone https://gitlab.com/lgo80/chat-socket-io.git".
1. Go to any terminal/console
1. Ir a la carpeta **chat-socket-io**
1. Execute the command **npm install**.
1. Execute the **npm start** command to run the server.
1. In a browser go to your localhost:3000 

**You have the chat up and running**
