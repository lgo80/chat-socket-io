const socket = io();

//DOM elements
let message = document.getElementById('message');
let username = document.getElementById('username');
let send = document.getElementById('send');
let output = document.getElementById('output');
let actions = document.getElementById('actions');
let enter = document.getElementById('enter');
let outputPrivate = document.getElementById('outputPrivate');
let messagePrivate = document.getElementById('messagePrivate');
let sendPrivate = document.getElementById('sendPrivate');
let chatUsers = document.getElementById('listUser');
let logInUser = document.getElementById('logInUser');
let pnlLogIn = document.getElementById('pnlLogIn');
let recipientUser = document.getElementById('recipientUser');

socket.on('chat:connections', function (room) {
    console.log((room.length == 0));
    chatUsers.innerHTML = '';
    if (room.length > 0) {
        room.forEach(function (room) {
            if (room.username) {
                chatUsers.innerHTML += `<a href="#" onclick="myFunction('${room.username}')" id="${room.username}">${room.username}</a>`;
            }
        });
    } else {
        chatUsers.innerHTML = `<p>No hay usuarios conectados en la sala 1</p>`;
    }
    actions.innerHTML += "<p>Alguien se conecto al servidor...</p>";
});

socket.on('chat:enter-out', function (data) {
    console.log(data.room);
    actions.innerHTML += data.message;
    var room = data.room;
    chatUsers.innerHTML = '';
    if (room && room.length > 0) {
        chatUsers.innerHTML += room.map(function(room){
            return `<a href="#" onclick="myFunction('${room.username}')" id="${room.username}">${room.username}</a>`;
        });
    }
});

socket.on('chat:enter-user', function (data) {
    logInUser.innerHTML = data.username;
    logInUser.classList.remove('hide');
    pnlLogIn.className = 'hide';

});

socket.on('chat:message', function (data) {
    output.innerHTML += `<p><strong>${data.username}</strong>: ${data.message}</p>`
});

socket.on('chat:typing', function (data) {
    actions.innerHTML = `<p><em>The user ${data} is writing a message</em></p>`;
});

socket.on('chat:private', function (data) {
    outputPrivate.innerHTML += `<p><strong>${data.username_emitter}</strong>: ${data.message}</p>`;
});

socket.on('chat:lala', function (data) {
    console.log("ES: " + data);
});

send.addEventListener('click', function () {
    if (logInUser.innerHTML != "Log in" && message.value) {
        socket.emit('chat:message', {
            username: username.value,
            message: message.value
        });
        message.value = "";
    }
});

message.addEventListener('keypress', function (event) {
    if (logInUser.innerHTML != "Log in") {
        socket.emit('chat:typing', username.value);
    }
    var codigo = event.key;
    if (codigo == 'Enter') {
        send.click();
    }
});

username.addEventListener('keypress', function (event) {
    var codigo = event.key;
    if (codigo == 'Enter') {
        enter.click();
    }
});

messagePrivate.addEventListener('keypress', function (event) {
    var codigo = event.key;
    if (codigo == 'Enter') {
        sendPrivate.click();
    }
});

enter.addEventListener('click', function () {
    socket.emit('chat:enter', {
        username: username.value
    });
});

sendPrivate.addEventListener('click', function () {
    if (logInUser.innerHTML != "Log in" && messagePrivate.value && recipientUser.innerHTML) {
        socket.emit('chat:private', {
            username_emitter: username.value,
            username_receiver: recipientUser.innerHTML,
            message: messagePrivate.value
        });
        messagePrivate.value = "";
    }
});

function myFunction(username) {
    recipientUser.innerHTML = username;
}